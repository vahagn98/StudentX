import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {CookieResolver} from "../util/cookie-resolver";
import {Injectable} from "@angular/core";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.endsWith('/login')) {
      let token = CookieResolver.getCookieValueByName('access_token');
      req = req.clone({
        headers: req.headers
          .set("Authorization", `Bearer ${token}`)
          .set("Content-Type", "application/json"),
      });
    } else {
      req = req.clone({
        withCredentials: true
      });
    }
    return next.handle(req);
  }

}
