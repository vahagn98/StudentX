import { Pipe, PipeTransform } from '@angular/core';
import {Observable} from 'rxjs';

@Pipe({
  name: 'attachmentResolver'
})
export class AttachmentResolverPipe implements PipeTransform {

  transform(urls: string[], ...args: any[]): any {
    return urls.reduce((acc, url) => acc += `<img src="${url}">`, '');
  }

}
