import {NgModule} from "@angular/core";
import {ConfirmationPopupComponent} from "./popup/confirmation-popup/confirmation-popup.component";
import {GeneralMaterialModulesModule} from "../general-material-modules.module";
import {AttachmentResolverPipe} from "./pipe/attachment-resolver.pipe";

@NgModule({
  declarations: [
    ConfirmationPopupComponent,
    AttachmentResolverPipe
  ],
  imports: [
    GeneralMaterialModulesModule
  ],
  providers: [
  ],
  entryComponents: [
    ConfirmationPopupComponent
  ],
  exports: [
    ConfirmationPopupComponent,
    AttachmentResolverPipe
  ]
})
export class SharedModule {
}
