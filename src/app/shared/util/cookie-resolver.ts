export class CookieResolver {
  private constructor(){}

  public static getCookieValueByName(name: string) {
    const position = document.cookie.indexOf(name);
    if (position === -1) {
      return '';
    }
    let end = document.cookie.indexOf(';', position);
    end === -1 ? end = document.cookie.length : end;
    return document.cookie.substring(position, end).split('=')[1];
  }

  public static setCookie(name: string, value: any, expires?: Date) {
    const expireDate = expires.toUTCString();
    document.cookie = `${name}=${value};expires=${expireDate};`;
  }

  public static removeCookie(name: string) {
    document.cookie = `${name}=;expires=Thu, 01 Jan 1970 00:00:00 GMT;`;
  }
}
