import {MatDialogConfig} from "@angular/material";

export class MatDialogUtil {
  private constructor(){}

  public static getDefaultDialogConfig(): MatDialogConfig {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.height = "auto";
    dialogConfig.width = "450px";
    return dialogConfig;
  }

  public static getDialogConfigWithData(data: any, width?: string, height?: string): MatDialogConfig{
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.height = height ? height : "auto";
    dialogConfig.width = width ? width : "450px";

    dialogConfig.data = data;
    return dialogConfig;
  }
}
