export abstract class ApplicationPropertiesService {
  /**
   * @param key
   * @returns property which is associated with this key
   */
  public abstract getProperty(key: string): Promise<string>;

  public abstract getProperties(keys: string[]): Promise<string[]>;

  public abstract getAllProperties(): Promise<any>;
}
