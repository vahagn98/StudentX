import {FileModel} from "./model/file-model";
import {Observable} from "rxjs";

export abstract class DocumentService {
  public abstract addFile(file: File): Observable<FileModel>;

  public abstract addFiles(files: File[]): Observable<FileModel[]>;

  public abstract deleteFile(fileId: string): Observable<FileModel>;

  public abstract loadFile(fileId: string): Observable<FileModel>;
}
