import {Observable} from "rxjs";

export abstract class AuthenticationService {
  abstract login(login: string, password: string): Observable<any>;
  abstract logout(): Observable<any>;
}
