import {Observable, of} from 'rxjs';
import {EventModel} from "./model/event-model";

export abstract class EventService {
  abstract loadEvent(eventId: number): Observable<EventModel>
  abstract loadEvents(): Observable<EventModel[]>
  abstract addEvent(event: EventModel): Observable<EventModel>
  abstract updateEvent(event: EventModel): Observable<EventModel>
  abstract deleteEvent(eventId: number): Observable<EventModel>
  public static createBlankEvent(): Observable<EventModel> {
    return of({
      id: null,
      title: null,
      attachments: [],
      description: null,
      date: null,
      content: null
    });
  }
}
