export interface EventModel {
  id: number,
  title: string,
  description: string,
  date: Date,
  content: any,
  attachments: string[]
}
