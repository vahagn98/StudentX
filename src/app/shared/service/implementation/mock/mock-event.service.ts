import {EventService} from "../../api/event.service";
import {Observable, of} from "rxjs";
import {EventModel} from "../../api/model/event-model";

export class MockEventService extends EventService{
  addEvent(event: EventModel): Observable<EventModel> {
    return undefined;
  }

  deleteEvent(eventId: number): Observable<EventModel> {
    return undefined;
  }

  loadEvent(eventId: number): Observable<EventModel> {
    return undefined;
  }

  loadEvents(): Observable<EventModel[]> {
    return of([{
      id: 1,
      title: 'Shiba Inu',
      description: 'gdfgsdfgsdf',
      date: new Date(),
      content: 'fghfghdf',
      attachments: ['https://material.angular.io/assets/img/examples/shiba2.jpg']
    }]);
  }

  updateEvent(event: EventModel): Observable<EventModel> {
    return undefined;
  }

}
