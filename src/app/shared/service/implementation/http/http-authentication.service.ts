import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthenticationService} from '../../api/authentication.service';
import {Router} from '@angular/router';
import {CookieResolver} from '../../../util/cookie-resolver';
import {from, Observable} from "rxjs";
import {ApplicationPropertiesService} from "../../api/application-properties.service";

@Injectable({
  providedIn: 'root'
})
export class HttpAuthenticationService extends AuthenticationService {

  constructor(private httpClient: HttpClient,
              private router: Router,
              private applicationPropertiesService: ApplicationPropertiesService) {
    super();
  }

  login(username: string, password: string): Observable<any> {
    return from(this.applicationPropertiesService.getProperties(['clientId', 'clientSecret'])
      .then(properties => btoa(`${properties[0]}:${properties[1]}`))
      .then(basicCredentials => this.loginToSystem(username, password, basicCredentials)));
  }

  logout(): Observable<any> {
    return from(this.applicationPropertiesService.getProperty('application-url')
      .then(url => this.httpClient.get(`${url}/logout`).toPromise())
      .then(() => CookieResolver.removeCookie("access_token"))
      .then(() => this.router.navigateByUrl('/login'))
      .catch(() => console.log('Logout was fail')));
  }

  private loginToSystem(username: string, password: string, basicCredentials) {
    const params = HttpAuthenticationService.createLoginParams(username, password, basicCredentials);
    const httpHeaders = HttpAuthenticationService.createLoginHeaders(basicCredentials);
    return this.applicationPropertiesService.getProperty('application-url')
      .then(url => this.httpClient.post(`${url}/oauth/token`, params, {headers: httpHeaders}).toPromise())
      .then((res: any) => HttpAuthenticationService.setToken(res))
      .then(() => this.router.navigateByUrl('/'))
      .catch((res: any) => console.log(res));
  }

  private static setToken(res: any) {
    const date = new Date(res['expires_in']);
    CookieResolver.setCookie("access_token", res['access_token'], new Date(Date.UTC(date.getFullYear(), date.getMonth(),
      date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds())));
  }

  private static createLoginHeaders(basisCredentials: string) {
    let httpHeaders: HttpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.set('Content-Type', 'application/x-www-form-urlencoded');
    httpHeaders = httpHeaders.set('Authorization', `Basic ${basisCredentials}`);
    return httpHeaders;
  }

  private static createLoginParams(username: string, password: string, basicCredentialsBase64: string) {
    const credentialsFromBase64 = atob(basicCredentialsBase64);
    const clientId = credentialsFromBase64.split(':')[0];
    return new HttpParams({
      fromObject: {
        grant_type: 'password',
        username: username,
        password: password,
        client_id: clientId
      }
    });
  }

}
