import { Injectable } from '@angular/core';
import {EventService} from '../../api/event.service';
import {from, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ApplicationPropertiesService} from "../../api/application-properties.service";
import {EventModel} from "../../api/model/event-model";

@Injectable({
  providedIn: 'root'
})
export class HttpEventService extends EventService {

  constructor(private httpClient: HttpClient,
              private propertiesService: ApplicationPropertiesService) {
    super();
  }

  public addEvent(event: EventModel): Observable<EventModel> {
    return from(this.propertiesService.getProperty('application-url')
      .then(url => this.httpClient.post<EventModel>(`${url}/event`, event).toPromise()));
  }

  public deleteEvent(eventId: number): Observable<EventModel> {
    return from(this.propertiesService.getProperty('application-url')
      .then(url => this.httpClient.delete<EventModel>(`${url}/event/${eventId}`).toPromise()));
  }

  public loadEvent(eventId: number): Observable<EventModel> {
    return from(this.propertiesService.getProperty('application-url')
      .then(url => this.httpClient.get<EventModel>(`${url}/event/${eventId}`).toPromise()));
  }

  public loadEvents(): Observable<EventModel[]> {
    return from(this.propertiesService.getProperty('application-url')
      .then(url => this.httpClient.get<EventModel[]>(`${url}/event`).toPromise()));
  }

  public updateEvent(event: EventModel): Observable<EventModel> {
    return from(this.propertiesService.getProperty('application-url')
      .then(url => this.httpClient.put<EventModel>(`${url}/event/${event.id}`, event).toPromise()));
  }
}
