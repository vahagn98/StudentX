import {ApplicationPropertiesService} from "../../api/application-properties.service";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root"
})
export class HttpApplicationPropertiesService extends ApplicationPropertiesService {
  private properties: any;

  constructor(private httpClient: HttpClient) {
    super();
  }

  public getProperty(key: string): Promise<string> {
    return this.getAllProperties().then(properties => properties[key]);
  }

  public getProperties(keys: string[]): Promise<string[]> {
    return this.getAllProperties()
      .then(properties => keys.map(key => properties[key]));
  }

  public getAllProperties(): Promise<any> {
    if (this.properties) {
      return Promise.resolve(this.properties);
    } else {
      return this.httpClient.get<string>(environment.pathToProperties).toPromise()
        .then(properties => this.properties = properties);
    }
  }
}
