import {DocumentService} from "../../api/document.service";
import {FileModel} from "../../api/model/file-model";
import {HttpClient} from "@angular/common/http";
import {combineLatest, from, Observable} from "rxjs";
import {ApplicationPropertiesService} from "../../api/application-properties.service";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class HttpDocumentService extends DocumentService {
  public constructor(private httpClient: HttpClient,
                     private applicationPropertiesService: ApplicationPropertiesService) {
    super();
  }

  addFile(file: File): Observable<FileModel> {
    const formData = new FormData();
    formData.set('file', file, file.name);
    return from(this.applicationPropertiesService.getProperty('document-service-url')
      .then(url => this.httpClient.post<any>(url, formData).toPromise())
      .then((response: any) => new FileModel(response.id, response.file, response.fileName)));
  }

  deleteFile(fileId: string): Observable<FileModel> {
    return from(this.applicationPropertiesService.getProperty('document-service-url')
      .then(url => this.httpClient.delete<any>(`${url}/${fileId}`).toPromise())
      .then((response: any) => new FileModel(response.id, response.file, response.fileName)));
  }

  loadFile(fileId: string): Observable<FileModel> {
    return from(this.applicationPropertiesService.getProperty('document-service-url')
      .then(url => this.httpClient.get<any>(`${url}/${fileId}`).toPromise())
      .then((response: any) => new FileModel(response.id, response.file, response.fileName)));
  }

  addFiles(files: File[]): Observable<FileModel[]> {
    return combineLatest(...files
      .map(file => this.addFile(file))
      .reduce((acc: any, file: Observable<FileModel>) => acc.push(file), []));
  }

}
