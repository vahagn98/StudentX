import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Component, Inject} from "@angular/core";

@Component({
  templateUrl: 'confirmation-popup.component.html',
  selector: 'confirmation-popup'
})
export class ConfirmationPopupComponent {
  public noMessage: string;
  public yesMessage: string;
  public title: string;
  public message: string;

  constructor(private matDialogRef: MatDialogRef<ConfirmationPopupComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any) {
    this.noMessage = data && data.noMessage ? data.noMessage : 'No';
    this.yesMessage = data && data.yesMessage ? data.yesMessage : 'Yes';
    this.title = data && data.title ? data.title : 'Warning';
    this.message = data && data.message ? data.message : 'Are you sure?';
  }

}
