import {Component, OnInit} from '@angular/core';
import {EventService} from '../shared/service/api/event.service';
import {combineLatest} from 'rxjs';
import {Router} from '@angular/router';
import {EventModel} from "../shared/service/api/model/event-model";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: []
})
export class HomeComponent implements OnInit {
  public isReady: boolean;
  public events: EventModel[];

  public constructor(private eventService: EventService,
                     private router: Router)  { }

  public ngOnInit() {
    const events$ = this.eventService.loadEvents();
    combineLatest(events$).subscribe(responses => {
      this.events = responses[0];
      this.isReady = true;
    });
  }

  public onReadMore(event: EventModel): void {
    this.router.navigateByUrl(`/event/view/${event.id}`);
  }

  public onAdd(): void {
    this.router.navigateByUrl(`/event/add`);
  }
}
