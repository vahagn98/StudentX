import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginPageComponent} from './login-page/login-page.component';
import {AppRoutingModule} from './app-routing.module';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {GeneralMaterialModulesModule} from './general-material-modules.module';
import {SharedModule} from "./shared/shared.module";
import {AuthenticationService} from "./shared/service/api/authentication.service";
import {HttpAuthenticationService} from "./shared/service/implementation/http/http-authentication.service";
import {ApplicationPropertiesService} from "./shared/service/api/application-properties.service";
import {HttpApplicationPropertiesService} from "./shared/service/implementation/http/http-application-properties.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule,
    BrowserAnimationsModule,
    GeneralMaterialModulesModule,
    SharedModule
  ],
  providers: [
    {provide: AuthenticationService, useClass: HttpAuthenticationService},
    {provide: ApplicationPropertiesService, useClass: HttpApplicationPropertiesService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
