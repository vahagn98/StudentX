import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {LoginPageComponent} from './login-page/login-page.component';

const routes: any = [
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: '',
    loadChildren: './shell/main.module#MainModule'
  },
  {
    path: '**',
    component: LoginPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule {

}
