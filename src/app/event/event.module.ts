import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventViewComponent} from './event-view/event-view.component';
import {EventEditComponent} from './event-edit/event-edit.component';
import {EventRoutingModule} from './event-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {GeneralMaterialModulesModule} from '../general-material-modules.module';


@NgModule({
  declarations: [
    EventViewComponent,
    EventEditComponent
  ],
  imports: [
    CommonModule,
    EventRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    GeneralMaterialModulesModule
  ]
})
export class EventModule { }
