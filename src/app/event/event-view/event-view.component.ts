import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../../shared/service/api/event.service';
import {EventModel} from "../../shared/service/api/model/event-model";

@Component({
  selector: 'event-view',
  templateUrl: './event-view.component.html',
  styleUrls: ['./event-view.component.css']
})
export class EventViewComponent implements OnInit {
  public isReady: boolean;
  public event: EventModel;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private eventService: EventService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => this.init(params['id']));
  }

  private init(eventId: number): void {
    this.eventService.loadEvent(eventId).subscribe(event => {
      this.event = event;
      this.isReady = true;
    });
  }

  public onDelete(): void {
    this.eventService.deleteEvent(this.event.id)
      .subscribe(() => this.router.navigateByUrl('/home'))
  }

  public onEdit(): void {
    this.router.navigateByUrl(`/event/edit/${this.event.id}`);
  }

  public onClose() {
    this.router.navigateByUrl('/home');
  }
}
