import {EventViewComponent} from './event-view/event-view.component';
import {EventEditComponent} from './event-edit/event-edit.component';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

const router = [
  {
    path: 'view/:id',
    component: EventViewComponent
  },
  {
    path: 'edit/:id',
    component: EventEditComponent
  },
  {
    path: 'add',
    component: EventEditComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(router)
  ]
})
export class EventRoutingModule {

}
