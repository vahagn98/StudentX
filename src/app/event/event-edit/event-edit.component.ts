import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventService} from '../../shared/service/api/event.service';
import {FormControl, Validators} from '@angular/forms';
import {EventModel} from "../../shared/service/api/model/event-model";
import {DocumentService} from "../../shared/service/api/document.service";

@Component({
  selector: 'event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.css']
})
export class EventEditComponent implements OnInit {
  public isReady: boolean;
  public event: EventModel;
  private files: File[] = [];
  private fileTypes: string[] = ["bmp", "gif", "img", "jpe", "jpeg", "jpg", "mac", "pbm", "pct", "png", "ppm", "psd", "tiff", "wmf", 'svg'];

  public eventControls: Map<string, FormControl> = new Map<string, FormControl>();

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private eventService: EventService,
              private documentService: DocumentService) {
  }

  ngOnInit() {
    this.addFormControls();
    this.activatedRoute.params.subscribe(params => this.init(params['id']));
  }

  private addFormControls() {
    this.eventControls.set('title', new FormControl('', [
      Validators.required
    ]));
    this.eventControls.set('description', new FormControl('', [
      Validators.required
    ]));
    this.eventControls.set('content', new FormControl('', [
      Validators.required
    ]));
    this.eventControls.set('file', new FormControl('', [
      Validators.required
    ]));
  }

  private init(eventId: number): void {
    let event$ = this.router.url.includes('/event/edit/') ?
      this.eventService.loadEvent(eventId) :
      EventService.createBlankEvent();
    event$.subscribe(event => {
      this.event = event;
      this.isReady = true;
    });
  }

  public onSave() {
    this.documentService.addFiles(this.files).toPromise()
      .then(fileModels => fileModels
        .map(fileModel => fileModel.id)
        .reduce((acc: any, fileId) => acc.push(fileId), []))
      .then((attachments: string[]) => this.event.attachments = attachments)
      .then(() => this.eventService.addEvent(this.event).toPromise())
      .then(event => this.router.navigateByUrl(`/event/edit/${event.id}`));
  }

  public onClose() {
    this.router.url.includes('/event/edit/') ?
      this.router.navigateByUrl(`/event/view/${this.event.id}`) :
      this.router.navigateByUrl('/home');
  }

  public onLoadFile(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      const file: File = files.item(i);
      if (file.type.startsWith("image") || this.fileTypes.some(fileType => file.name.substr(file.name.lastIndexOf('.')) === fileType)) {
        this.files.push(file);
      } else {
        this.files = [];
        throw new Error(`${file.name} has ${file.type} extension witch is not picture type.`);
      }
    }
  }
}
