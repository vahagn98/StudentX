import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthenticationService} from '../shared/service/api/authentication.service';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  private loginFormGroup: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService) {
    this.createForm();
  }

  ngOnInit() {
  }

  private createForm(): void {
    this.loginFormGroup = this.formBuilder.group({
      email: [''],
      password: ['']
    })
  }

  onClickSubmit(email: string, password: string) {
    this.authenticationService.login(email, password);
  }

}
