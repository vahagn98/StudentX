import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {AuthenticationService} from "../shared/service/api/authentication.service";
import {ConfirmationPopupComponent} from "../shared/popup/confirmation-popup/confirmation-popup.component";
import {MatDialogUtil} from "../shared/util/mat-dialog-util";

@Component({
  selector: 'shell',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  constructor(private router: Router,
              private authService: AuthenticationService,
              private matDialog: MatDialog){}

  ngOnInit(): void {
  }

  login() {
    this.router.navigateByUrl("/login");
  }

  logout() {
    this.matDialog.open(ConfirmationPopupComponent, MatDialogUtil.getDefaultDialogConfig()).afterClosed()
      .subscribe((isLogout: boolean) => isLogout && this.authService.logout());
  }
}
