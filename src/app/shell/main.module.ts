import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HomeComponent} from "../home/home.component";
import {HomeworkComponent} from "../homework/homework.component";
import {GeneralMaterialModulesModule} from "../general-material-modules.module";
import {SharedModule} from "../shared/shared.module";
import {MainComponent} from "./main.component";
import {MainRoutingModule} from "./main-routing.module";
import {CommonModule} from "@angular/common";
import {AuthenticationService} from "../shared/service/api/authentication.service";
import {HttpEventService} from "../shared/service/implementation/http/http-event.service";
import {HttpApplicationPropertiesService} from "../shared/service/implementation/http/http-application-properties.service";
import {EventService} from "../shared/service/api/event.service";
import {ApplicationPropertiesService} from "../shared/service/api/application-properties.service";
import {TokenInterceptor} from "../shared/interceptor/custom-interceptor.service";
import {HttpAuthenticationService} from "../shared/service/implementation/http/http-authentication.service";
import {DocumentService} from "../shared/service/api/document.service";
import {HttpDocumentService} from "../shared/service/implementation/http/http-document.service";

@NgModule({
  declarations: [
    MainComponent,
    HomeworkComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MainRoutingModule,
    HttpClientModule,
    GeneralMaterialModulesModule,
    SharedModule
  ],
  providers: [
    {provide: AuthenticationService, useClass: HttpAuthenticationService},
    {provide: EventService, useClass: HttpEventService},
    {provide: ApplicationPropertiesService, useClass: HttpApplicationPropertiesService},
    {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true},
    {provide: DocumentService, useClass: HttpDocumentService}
  ]
})
export class MainModule { }
