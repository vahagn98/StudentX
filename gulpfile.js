const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const tildeImporter = require('node-sass-tilde-importer');

gulp.task('sass', () => {
  return gulp.src(['src/app/**/*.scss', 'src/styles.scss'], {base: './'})
    .pipe(sass({includePaths: ["node_modules/"], importer: tildeImporter}).on('error', sass.logError))
    .pipe(gulp.dest('./'));
});
